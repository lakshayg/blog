# Relation between the Pascal's and the Sierpinski's triangle

Last updated: 2020-04-15

While working on the project euler problem 148, I came across something cool.
Here's the mathematica code to demonstrate it.

```
MatrixPlot[
 Table[Mod[Binomial[n, Range[0, n]], 5], {n, 0, 74}],
 Frame -> False, ColorFunction -> "Monochrome"]
```

Basically, (n choose k) mod p for some prime number p. Plotting this generates
the Sierpinki's triangle.

<img src="https://user-images.githubusercontent.com/7976315/79319728-8e53d980-7ebd-11ea-9d06-327a02b173b2.png" width="400px"/>
