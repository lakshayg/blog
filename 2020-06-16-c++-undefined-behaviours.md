### Sequence points

```cpp
int i = 0;

// undefined behaviour because i is modified
// more than once between two sequence points
int j = ++i + ++i;
```

- https://en.wikipedia.org/wiki/Sequence_point
- https://stackoverflow.com/a/4176333/3033441
- https://stackoverflow.com/a/4183735/3033441

### Trap representation

From cppreference:

> If an object representation does not represent any value of the
> object type, it is known as trap representation. Accessing a
> trap representation in any way other than reading it through an
> lvalue expression of character type is undefined behavior

- https://stackoverflow.com/questions/6725809/trap-representation
