| Date | Post |
|------|------|
| 2020-06-16 | [Interesting UBs in C++](2020-06-16-c++-undefined-behaviours.md) |
| 2020-04-15 | [Pascal's and Sierpinski's Triangle](2020-04-15-pascal-and-sierpinski-triangle.md) |
| 2020-04-07 | [Mathematica plugin for TeXmacs](2020-04-07-texmacs-mathematica-plugin.md) |
